from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

import requests

from .serializers import CreateUserSerializer

CLIENT_ID = 'TRc3uIWTqDTeMNT4lBkYmzr6MhUNLfswE26hDOhJ'
CLIENT_SECRET = 'sdCADfEzXbkhmHBJR6k7jvegyrMXXZxLEXKtr9KeMpnfRva4ike3TjcbD1WJTklPGO0bti8Xm8HlQoVN6OzJCWtpza80NORTYdvBBAEMZvGpnLnN55FaEDre3RiflAdz'

@api_view(['POST'])
@permission_classes([AllowAny])
def register_username_password(request):
    serializer = CreateUserSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        r = requests.post('http://0.0.0.0:8000/o/token',
            data ={
                'grant_type': 'password',
                'username': request.data['username'],
                'password': request.data['password'],
                'client_id': CLIENT_ID,
                'client_secret': CLIENT_SECRET,
            },
        )
        return Response(r.json())
    return Response(serializer.errors)


@api_view(['POST'])
@permission_classes([AllowAny])
def authenticate_username_password(request):
    r = requests.post('http://0.0.0.0:8000/o/token/',
        data={
            'grant_type': 'password',
            'username': request.data['username'],
            'password': request.data['password'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    return Response(r.json())


@api_view(['POST'])
@permission_classes([AllowAny])
def refresh_token(request):
    r = requests.post('http://0.0.0.0:8000/o/token/',
        data={
            'grant_type': 'refresh_token',
            'refresh_token': request.data['refresh_token'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )
    return Response(r.json())


@api_view(['POST'])
@permission_classes([AllowAny])
def unauthenticate_token(request):
    r = request.post('http://0.0.0.0:8000/o/revoke_token/',
        data={
            'token': request.data['token'],
            'client_id': CLIENT_ID,
            'client_secret': CLIENT_SECRET,
        },
    )

    if r.status_code == requests.codes.ok:
        return Response({'success': 'Token unauthenticated'}, r.status_code)
    return Response(r.json(), r.status_code)

