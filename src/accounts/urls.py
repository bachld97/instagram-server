from django.urls import path

from . import views

urlpatterns = [
    path('register/', views.register_username_password),
    path('authenticate/', views.authenticate_username_password),
    path('token/refresh/', views.refresh_token),
    path('token/revoke/', views.unauthenticate_token),
]
