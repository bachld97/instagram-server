from django.db import models
from rest_framework import serializers
from api import settings

import datetime

class Post(models.Model):
    creator = models.ForeignKey(
        User, on_delete=models.CASCADE
    )
    created_at = models.DateTimeFields(
        default=datetime.datetime.now
    )
    like_count = models.IntegerField()
    caption = models.CharField(max_length=255, blank=True)

    class Meta:
        ordering = ['like_count']


class PostContent(models.Model):
    TYPE_VIDEO = 'vi'
    TYPE_IMAGE = 'im'

    TYPE_CHOICES = (
        (TYPE_VIDEO, 'Video'),
        (TYPE_IMAGE, 'Image'),
    )

    post_id = models.ForeignKey(
        Post, on_delete=models.CASCADE
    )
    file_location = models.FileField(blank=False, null=False)
    content_type = models.CharField(
        max_length=2,
        choices = TYPE_CHOICES,
        default=TYPE_IMAGE
    )


class PostContentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostContent
        fields = '__all_'


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = post
        fields = ('creator', 'created_at', 'like_count', 'caption')
