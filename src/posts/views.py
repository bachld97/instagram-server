from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from .models import PostContentSerializer

# Create your views here.
class PostUploadView(APIView):
    parser_class = (FileUploadParser,)

    def post(self, request, *args, **kwargs):

        creator = request.user
        caption = request.data['caption']
        post_dict = { 'creator': creator, 'caption': caption }
        post_serializer = PostSerializer(data=post_dict)

        post_content_serializer = PostContentSerializer(
            data=request.data['content']
        )

        # if (post_serializer.is_valid() and
        #         post_content_serializer.is_valid()):
        #     return Responst()

        if file_serializer.is_valid():
            file_serializer.save()
            return Response(
                file_serializer.data, status=status.HTTP_201_CREATED
            )
        else:
            return Response(
                file_serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
